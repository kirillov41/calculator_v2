#include <QApplication>
#include "./ui_calc.h"
#include "calc.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Calc window(nullptr);
    Ui::MainWindow calc;
    calc.setupUi(&window);
    window.text1 = calc.text1;
    window.text2 = calc.text2;
    window.operation = calc.operation;
    window.result = calc.result;

    window.show();
    return a.exec();
}
