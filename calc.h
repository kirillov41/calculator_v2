#pragma once
#include <QObject>
#include <string>
#include <QMainWindow>
#include <QLineEdit>

class Calc : public QMainWindow
{
    Q_OBJECT
private:
    bool isEquals = false;
    double sum = 0;
    void setValue(QLineEdit* value, QString val){
        if (val== "." && value->text().toStdString().find(".")!=std::string::npos) return;
        if (value->text()=="0" && val != "-")
            if (val == ".") value->setText(value->text()+= val);
            else value->setText(val);
        else if (val == "-" && value->text()!="0"){
            std::string tmp = value->text().toStdString();
            if (tmp.find("-")==std::string::npos){
                tmp.insert(0,"-");
                value->setText(QString::fromStdString(tmp));
            }else {
                tmp.erase(0,1);
                value->setText(QString::fromStdString(tmp));
            }
        }
        else if (value->text().size() < 10 && val != "-") value->setText(value->text()+= val);
    }

    void addNum(QString val) {
        if (isEquals) return;
            if (operation->text() == ""){
                setValue(text1, val);
            }
            else {
                setValue(text2, val);
            }
        }
    void getSum(){
        if (operation->text() == "") return;
        isEquals = true;
        double val1 = std::stod(text1->text().toStdString());
        double val2 = std::stod(text2->text().toStdString());
        if (operation->text() == "+")
            sum = val1 + val2;
        else if (operation->text() == "-")
            sum = val1 - val2;
        else if (operation->text() == "*")
            sum = val1 * val2;
        else if (operation->text() == "/"){
            if (text2->text()=="0"){

                result->setText("ERROR");
                return;
            }else sum = val1 / val2;
        }
        int sumInt = sum;
        if (sum - sumInt != 0) result->setText(QString::fromStdString(std::to_string(sum)));
        else result->setText(QString::fromStdString(std::to_string(sumInt)));
    }
public:

    QLineEdit* text1 = nullptr;
    QLineEdit* text2 = nullptr;
    QLineEdit* operation = nullptr;
    QLineEdit* result = {nullptr};

    Calc(QWidget* parrent = nullptr) : QMainWindow(parrent){

    }

public slots:

    void addNum1(){addNum("1");}
    void addNum2(){addNum("2");}
    void addNum3(){addNum("3");}
    void addNum4(){addNum("4");}
    void addNum5(){addNum("5");}
    void addNum6(){addNum("6");}
    void addNum7(){addNum("7");}
    void addNum8(){addNum("8");}
    void addNum9(){addNum("9");}
    void addNum0(){addNum("0");}
    void reset(){
        text1->setText("0");
        text2->setText("0");
        sum = 0;
        operation->setText("");
        result->setText("0");
        isEquals = false;
    }
    void plus(){setOperation("+");}
    void minus(){setOperation("-");}
    void multiply(){setOperation("*");}
    void divide(){setOperation("/");}
    void equals(){getSum();}
    void dot(){addNum(".");}
    void minusSign(){addNum("-");}

    void setOperation(QString val){
        if (operation->text()==""){
            operation->setText(val);
        }
    }

};

